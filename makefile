SOURCE:=source
BUILD :=build
PREFIX:=/project/python



# ---

.PHONY: default
default: clean base control environment package template


.PHONY: base
base: $(addprefix $(BUILD)/, base yaml project version initialize)
	chmod a+x $(BUILD)/version
	chmod a+x $(BUILD)/initialize


.PHONY: control
control: $(addprefix $(BUILD)/control/, test run)
	chmod a+x $(BUILD)/control/test
	chmod a+x $(BUILD)/control/run


.PHONY: package
package: $(addprefix $(BUILD)/control/package/, build)
	chmod a+x $(BUILD)/control/package/build


.PHONY: environment
environment: $(addprefix $(BUILD)/control/environment/, activate create link save)
	chmod a+x $(BUILD)/control/environment/create
	chmod a+x $(BUILD)/control/environment/link
	chmod a+x $(BUILD)/control/environment/save

	cp $(SOURCE)/control/environment/getsite.py $(BUILD)/control/environment/getsite.py


.PHONY: template
template:
	cp -r $(SOURCE)/template/ $(BUILD)/


# ---

$(BUILD)/%: $(SOURCE)/%
	mkdir -p $(@D)
	control/substitute "$(PREFIX)" $< $@


.PHONY: clean
clean:
	mkdir -p $(BUILD)
	rm -rf $(BUILD)/*


# ---

.PHONY: .SILENT



# ---

.PHONY: install
install:
	mkdir -p $(PREFIX)
	cp -r build/* $(PREFIX)/


.PHONY: uninstall
uninstall:
	rm -rf $(PREFIX)


.PHONY: reinstall
reinstall: uninstall default install
